# ETP Java dev kit
  
Generated Java classes for the ETP v1.2 avro protocol.

**NOTE** the junit test fails after the avro schema has been updated, since all of the generated source files need to be updated as well.
See [etp-java-schema-only](https://bitbucket.org/jamesVelasco/etp-java-schema-only) for a simpler approach which only requires updating the avro schema.

## Requirements

- JDK 8 or later. 
- Maven 3.0 or later.

## Compilation and usage

- Use `mvn test` to compile the ETP avro schema Java class sources and validate them against the avro ETP protocol schema,
  from which they were generated from.

- Use `mvn package` to create a jar file at `target/etp-java-1.2-<VERSION>.jar`.
