package org.energistics.etp;

import Energistics.Etp.v12.*;
import org.apache.avro.Protocol;

// JUnit dependencies
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

import java.io.*;

/**
 * Validates generation of Avro schema fielsnd compilation
 */
@RunWith(JUnit4.class)
public final class EtpTest
{
    private static final String PROTOCOL_SCHEMA = "src/main/avro/etp.avpr";

    @Test public void testEtpProtocol() throws Exception
    {
        Protocol generated = Etp.PROTOCOL;
        assertNotNull( generated );

        Protocol parsed = Protocol.parse( new File( PROTOCOL_SCHEMA ) );
        assertNotNull( parsed );

        assertEquals( generated, parsed );
    }

}
