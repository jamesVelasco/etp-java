/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package Energistics.Etp.v12.Datatypes.Object;

import org.apache.avro.specific.SpecificData;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@SuppressWarnings("all")
@org.apache.avro.specific.AvroGenerated
public class OptionalDataObject extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = -1265308833704209833L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"OptionalDataObject\",\"namespace\":\"Energistics.Etp.v12.Datatypes.Object\",\"fields\":[{\"name\":\"resource\",\"type\":{\"type\":\"record\",\"name\":\"Resource\",\"fields\":[{\"name\":\"uri\",\"type\":\"string\"},{\"name\":\"contentType\",\"type\":\"string\"},{\"name\":\"name\",\"type\":\"string\"},{\"name\":\"objectNotifiable\",\"type\":\"boolean\",\"default\":true},{\"name\":\"resourceType\",\"type\":{\"type\":\"enum\",\"name\":\"ResourceKind\",\"symbols\":[\"DataObject\",\"Folder\",\"UriProtocol\",\"DataSpace\"],\"fullName\":\"Energistics.Etp.v12.Datatypes.Object.ResourceKind\",\"depends\":[]}},{\"name\":\"sourceCount\",\"type\":[\"null\",\"int\"],\"default\":null},{\"name\":\"targetCount\",\"type\":[\"null\",\"int\"],\"default\":null},{\"name\":\"contentCount\",\"type\":[\"null\",\"int\"],\"default\":null},{\"name\":\"lastChanged\",\"type\":[\"null\",\"long\"]},{\"name\":\"customData\",\"type\":{\"type\":\"map\",\"values\":\"string\"}}],\"fullName\":\"Energistics.Etp.v12.Datatypes.Object.Resource\",\"depends\":[\"Energistics.Etp.v12.Datatypes.Object.ResourceKind\"]}},{\"name\":\"data\",\"type\":[\"null\",\"bytes\"]}],\"fullName\":\"Energistics.Etp.v12.Datatypes.Object.OptionalDataObject\",\"depends\":[\"Energistics.Etp.v12.Datatypes.Object.Resource\"]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<OptionalDataObject> ENCODER =
      new BinaryMessageEncoder<OptionalDataObject>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<OptionalDataObject> DECODER =
      new BinaryMessageDecoder<OptionalDataObject>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   */
  public static BinaryMessageDecoder<OptionalDataObject> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   */
  public static BinaryMessageDecoder<OptionalDataObject> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<OptionalDataObject>(MODEL$, SCHEMA$, resolver);
  }

  /** Serializes this OptionalDataObject to a ByteBuffer. */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /** Deserializes a OptionalDataObject from a ByteBuffer. */
  public static OptionalDataObject fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

  @Deprecated public Energistics.Etp.v12.Datatypes.Object.Resource resource;
  @Deprecated public java.nio.ByteBuffer data;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public OptionalDataObject() {}

  /**
   * All-args constructor.
   * @param resource The new value for resource
   * @param data The new value for data
   */
  public OptionalDataObject(Energistics.Etp.v12.Datatypes.Object.Resource resource, java.nio.ByteBuffer data) {
    this.resource = resource;
    this.data = data;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return resource;
    case 1: return data;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: resource = (Energistics.Etp.v12.Datatypes.Object.Resource)value$; break;
    case 1: data = (java.nio.ByteBuffer)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'resource' field.
   * @return The value of the 'resource' field.
   */
  public Energistics.Etp.v12.Datatypes.Object.Resource getResource() {
    return resource;
  }

  /**
   * Sets the value of the 'resource' field.
   * @param value the value to set.
   */
  public void setResource(Energistics.Etp.v12.Datatypes.Object.Resource value) {
    this.resource = value;
  }

  /**
   * Gets the value of the 'data' field.
   * @return The value of the 'data' field.
   */
  public java.nio.ByteBuffer getData() {
    return data;
  }

  /**
   * Sets the value of the 'data' field.
   * @param value the value to set.
   */
  public void setData(java.nio.ByteBuffer value) {
    this.data = value;
  }

  /**
   * Creates a new OptionalDataObject RecordBuilder.
   * @return A new OptionalDataObject RecordBuilder
   */
  public static Energistics.Etp.v12.Datatypes.Object.OptionalDataObject.Builder newBuilder() {
    return new Energistics.Etp.v12.Datatypes.Object.OptionalDataObject.Builder();
  }

  /**
   * Creates a new OptionalDataObject RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new OptionalDataObject RecordBuilder
   */
  public static Energistics.Etp.v12.Datatypes.Object.OptionalDataObject.Builder newBuilder(Energistics.Etp.v12.Datatypes.Object.OptionalDataObject.Builder other) {
    return new Energistics.Etp.v12.Datatypes.Object.OptionalDataObject.Builder(other);
  }

  /**
   * Creates a new OptionalDataObject RecordBuilder by copying an existing OptionalDataObject instance.
   * @param other The existing instance to copy.
   * @return A new OptionalDataObject RecordBuilder
   */
  public static Energistics.Etp.v12.Datatypes.Object.OptionalDataObject.Builder newBuilder(Energistics.Etp.v12.Datatypes.Object.OptionalDataObject other) {
    return new Energistics.Etp.v12.Datatypes.Object.OptionalDataObject.Builder(other);
  }

  /**
   * RecordBuilder for OptionalDataObject instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<OptionalDataObject>
    implements org.apache.avro.data.RecordBuilder<OptionalDataObject> {

    private Energistics.Etp.v12.Datatypes.Object.Resource resource;
    private Energistics.Etp.v12.Datatypes.Object.Resource.Builder resourceBuilder;
    private java.nio.ByteBuffer data;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(Energistics.Etp.v12.Datatypes.Object.OptionalDataObject.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.resource)) {
        this.resource = data().deepCopy(fields()[0].schema(), other.resource);
        fieldSetFlags()[0] = true;
      }
      if (other.hasResourceBuilder()) {
        this.resourceBuilder = Energistics.Etp.v12.Datatypes.Object.Resource.newBuilder(other.getResourceBuilder());
      }
      if (isValidValue(fields()[1], other.data)) {
        this.data = data().deepCopy(fields()[1].schema(), other.data);
        fieldSetFlags()[1] = true;
      }
    }

    /**
     * Creates a Builder by copying an existing OptionalDataObject instance
     * @param other The existing instance to copy.
     */
    private Builder(Energistics.Etp.v12.Datatypes.Object.OptionalDataObject other) {
            super(SCHEMA$);
      if (isValidValue(fields()[0], other.resource)) {
        this.resource = data().deepCopy(fields()[0].schema(), other.resource);
        fieldSetFlags()[0] = true;
      }
      this.resourceBuilder = null;
      if (isValidValue(fields()[1], other.data)) {
        this.data = data().deepCopy(fields()[1].schema(), other.data);
        fieldSetFlags()[1] = true;
      }
    }

    /**
      * Gets the value of the 'resource' field.
      * @return The value.
      */
    public Energistics.Etp.v12.Datatypes.Object.Resource getResource() {
      return resource;
    }

    /**
      * Sets the value of the 'resource' field.
      * @param value The value of 'resource'.
      * @return This builder.
      */
    public Energistics.Etp.v12.Datatypes.Object.OptionalDataObject.Builder setResource(Energistics.Etp.v12.Datatypes.Object.Resource value) {
      validate(fields()[0], value);
      this.resourceBuilder = null;
      this.resource = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'resource' field has been set.
      * @return True if the 'resource' field has been set, false otherwise.
      */
    public boolean hasResource() {
      return fieldSetFlags()[0];
    }

    /**
     * Gets the Builder instance for the 'resource' field and creates one if it doesn't exist yet.
     * @return This builder.
     */
    public Energistics.Etp.v12.Datatypes.Object.Resource.Builder getResourceBuilder() {
      if (resourceBuilder == null) {
        if (hasResource()) {
          setResourceBuilder(Energistics.Etp.v12.Datatypes.Object.Resource.newBuilder(resource));
        } else {
          setResourceBuilder(Energistics.Etp.v12.Datatypes.Object.Resource.newBuilder());
        }
      }
      return resourceBuilder;
    }

    /**
     * Sets the Builder instance for the 'resource' field
     * @param value The builder instance that must be set.
     * @return This builder.
     */
    public Energistics.Etp.v12.Datatypes.Object.OptionalDataObject.Builder setResourceBuilder(Energistics.Etp.v12.Datatypes.Object.Resource.Builder value) {
      clearResource();
      resourceBuilder = value;
      return this;
    }

    /**
     * Checks whether the 'resource' field has an active Builder instance
     * @return True if the 'resource' field has an active Builder instance
     */
    public boolean hasResourceBuilder() {
      return resourceBuilder != null;
    }

    /**
      * Clears the value of the 'resource' field.
      * @return This builder.
      */
    public Energistics.Etp.v12.Datatypes.Object.OptionalDataObject.Builder clearResource() {
      resource = null;
      resourceBuilder = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'data' field.
      * @return The value.
      */
    public java.nio.ByteBuffer getData() {
      return data;
    }

    /**
      * Sets the value of the 'data' field.
      * @param value The value of 'data'.
      * @return This builder.
      */
    public Energistics.Etp.v12.Datatypes.Object.OptionalDataObject.Builder setData(java.nio.ByteBuffer value) {
      validate(fields()[1], value);
      this.data = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'data' field has been set.
      * @return True if the 'data' field has been set, false otherwise.
      */
    public boolean hasData() {
      return fieldSetFlags()[1];
    }


    /**
      * Clears the value of the 'data' field.
      * @return This builder.
      */
    public Energistics.Etp.v12.Datatypes.Object.OptionalDataObject.Builder clearData() {
      data = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public OptionalDataObject build() {
      try {
        OptionalDataObject record = new OptionalDataObject();
        if (resourceBuilder != null) {
          record.resource = this.resourceBuilder.build();
        } else {
          record.resource = fieldSetFlags()[0] ? this.resource : (Energistics.Etp.v12.Datatypes.Object.Resource) defaultValue(fields()[0]);
        }
        record.data = fieldSetFlags()[1] ? this.data : (java.nio.ByteBuffer) defaultValue(fields()[1]);
        return record;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<OptionalDataObject>
    WRITER$ = (org.apache.avro.io.DatumWriter<OptionalDataObject>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<OptionalDataObject>
    READER$ = (org.apache.avro.io.DatumReader<OptionalDataObject>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

}
