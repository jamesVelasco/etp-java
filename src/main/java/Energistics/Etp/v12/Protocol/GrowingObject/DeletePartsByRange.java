/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package Energistics.Etp.v12.Protocol.GrowingObject;

import org.apache.avro.specific.SpecificData;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@SuppressWarnings("all")
@org.apache.avro.specific.AvroGenerated
public class DeletePartsByRange extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = -6642077316615427913L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"DeletePartsByRange\",\"namespace\":\"Energistics.Etp.v12.Protocol.GrowingObject\",\"fields\":[{\"name\":\"uri\",\"type\":\"string\"},{\"name\":\"deleteInterval\",\"type\":{\"type\":\"record\",\"name\":\"IndexInterval\",\"namespace\":\"Energistics.Etp.v12.Datatypes.Object\",\"fields\":[{\"name\":\"startIndex\",\"type\":{\"type\":\"record\",\"name\":\"IndexValue\",\"namespace\":\"Energistics.Etp.v12.Datatypes\",\"fields\":[{\"name\":\"item\",\"type\":[\"null\",\"long\",\"double\"]}],\"fullName\":\"Energistics.Etp.v12.Datatypes.IndexValue\",\"depends\":[]}},{\"name\":\"endIndex\",\"type\":\"Energistics.Etp.v12.Datatypes.IndexValue\"},{\"name\":\"uom\",\"type\":\"string\"},{\"name\":\"depthDatum\",\"type\":\"string\",\"default\":\"\"}],\"fullName\":\"Energistics.Etp.v12.Datatypes.Object.IndexInterval\",\"depends\":[\"Energistics.Etp.v12.Datatypes.IndexValue\",\"Energistics.Etp.v12.Datatypes.IndexValue\"]}},{\"name\":\"includeOverlappingIntervals\",\"type\":\"boolean\"}],\"protocol\":\"6\",\"messageType\":\"2\",\"senderRole\":\"customer\",\"protocolRoles\":\"store,customer\",\"multipartFlag\":false,\"fullName\":\"Energistics.Etp.v12.Protocol.GrowingObject.DeletePartsByRange\",\"depends\":[\"Energistics.Etp.v12.Datatypes.Object.IndexInterval\"]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<DeletePartsByRange> ENCODER =
      new BinaryMessageEncoder<DeletePartsByRange>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<DeletePartsByRange> DECODER =
      new BinaryMessageDecoder<DeletePartsByRange>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   */
  public static BinaryMessageDecoder<DeletePartsByRange> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   */
  public static BinaryMessageDecoder<DeletePartsByRange> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<DeletePartsByRange>(MODEL$, SCHEMA$, resolver);
  }

  /** Serializes this DeletePartsByRange to a ByteBuffer. */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /** Deserializes a DeletePartsByRange from a ByteBuffer. */
  public static DeletePartsByRange fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

  @Deprecated public java.lang.CharSequence uri;
  @Deprecated public Energistics.Etp.v12.Datatypes.Object.IndexInterval deleteInterval;
  @Deprecated public boolean includeOverlappingIntervals;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public DeletePartsByRange() {}

  /**
   * All-args constructor.
   * @param uri The new value for uri
   * @param deleteInterval The new value for deleteInterval
   * @param includeOverlappingIntervals The new value for includeOverlappingIntervals
   */
  public DeletePartsByRange(java.lang.CharSequence uri, Energistics.Etp.v12.Datatypes.Object.IndexInterval deleteInterval, java.lang.Boolean includeOverlappingIntervals) {
    this.uri = uri;
    this.deleteInterval = deleteInterval;
    this.includeOverlappingIntervals = includeOverlappingIntervals;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return uri;
    case 1: return deleteInterval;
    case 2: return includeOverlappingIntervals;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: uri = (java.lang.CharSequence)value$; break;
    case 1: deleteInterval = (Energistics.Etp.v12.Datatypes.Object.IndexInterval)value$; break;
    case 2: includeOverlappingIntervals = (java.lang.Boolean)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'uri' field.
   * @return The value of the 'uri' field.
   */
  public java.lang.CharSequence getUri() {
    return uri;
  }

  /**
   * Sets the value of the 'uri' field.
   * @param value the value to set.
   */
  public void setUri(java.lang.CharSequence value) {
    this.uri = value;
  }

  /**
   * Gets the value of the 'deleteInterval' field.
   * @return The value of the 'deleteInterval' field.
   */
  public Energistics.Etp.v12.Datatypes.Object.IndexInterval getDeleteInterval() {
    return deleteInterval;
  }

  /**
   * Sets the value of the 'deleteInterval' field.
   * @param value the value to set.
   */
  public void setDeleteInterval(Energistics.Etp.v12.Datatypes.Object.IndexInterval value) {
    this.deleteInterval = value;
  }

  /**
   * Gets the value of the 'includeOverlappingIntervals' field.
   * @return The value of the 'includeOverlappingIntervals' field.
   */
  public java.lang.Boolean getIncludeOverlappingIntervals() {
    return includeOverlappingIntervals;
  }

  /**
   * Sets the value of the 'includeOverlappingIntervals' field.
   * @param value the value to set.
   */
  public void setIncludeOverlappingIntervals(java.lang.Boolean value) {
    this.includeOverlappingIntervals = value;
  }

  /**
   * Creates a new DeletePartsByRange RecordBuilder.
   * @return A new DeletePartsByRange RecordBuilder
   */
  public static Energistics.Etp.v12.Protocol.GrowingObject.DeletePartsByRange.Builder newBuilder() {
    return new Energistics.Etp.v12.Protocol.GrowingObject.DeletePartsByRange.Builder();
  }

  /**
   * Creates a new DeletePartsByRange RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new DeletePartsByRange RecordBuilder
   */
  public static Energistics.Etp.v12.Protocol.GrowingObject.DeletePartsByRange.Builder newBuilder(Energistics.Etp.v12.Protocol.GrowingObject.DeletePartsByRange.Builder other) {
    return new Energistics.Etp.v12.Protocol.GrowingObject.DeletePartsByRange.Builder(other);
  }

  /**
   * Creates a new DeletePartsByRange RecordBuilder by copying an existing DeletePartsByRange instance.
   * @param other The existing instance to copy.
   * @return A new DeletePartsByRange RecordBuilder
   */
  public static Energistics.Etp.v12.Protocol.GrowingObject.DeletePartsByRange.Builder newBuilder(Energistics.Etp.v12.Protocol.GrowingObject.DeletePartsByRange other) {
    return new Energistics.Etp.v12.Protocol.GrowingObject.DeletePartsByRange.Builder(other);
  }

  /**
   * RecordBuilder for DeletePartsByRange instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<DeletePartsByRange>
    implements org.apache.avro.data.RecordBuilder<DeletePartsByRange> {

    private java.lang.CharSequence uri;
    private Energistics.Etp.v12.Datatypes.Object.IndexInterval deleteInterval;
    private Energistics.Etp.v12.Datatypes.Object.IndexInterval.Builder deleteIntervalBuilder;
    private boolean includeOverlappingIntervals;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(Energistics.Etp.v12.Protocol.GrowingObject.DeletePartsByRange.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.uri)) {
        this.uri = data().deepCopy(fields()[0].schema(), other.uri);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.deleteInterval)) {
        this.deleteInterval = data().deepCopy(fields()[1].schema(), other.deleteInterval);
        fieldSetFlags()[1] = true;
      }
      if (other.hasDeleteIntervalBuilder()) {
        this.deleteIntervalBuilder = Energistics.Etp.v12.Datatypes.Object.IndexInterval.newBuilder(other.getDeleteIntervalBuilder());
      }
      if (isValidValue(fields()[2], other.includeOverlappingIntervals)) {
        this.includeOverlappingIntervals = data().deepCopy(fields()[2].schema(), other.includeOverlappingIntervals);
        fieldSetFlags()[2] = true;
      }
    }

    /**
     * Creates a Builder by copying an existing DeletePartsByRange instance
     * @param other The existing instance to copy.
     */
    private Builder(Energistics.Etp.v12.Protocol.GrowingObject.DeletePartsByRange other) {
            super(SCHEMA$);
      if (isValidValue(fields()[0], other.uri)) {
        this.uri = data().deepCopy(fields()[0].schema(), other.uri);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.deleteInterval)) {
        this.deleteInterval = data().deepCopy(fields()[1].schema(), other.deleteInterval);
        fieldSetFlags()[1] = true;
      }
      this.deleteIntervalBuilder = null;
      if (isValidValue(fields()[2], other.includeOverlappingIntervals)) {
        this.includeOverlappingIntervals = data().deepCopy(fields()[2].schema(), other.includeOverlappingIntervals);
        fieldSetFlags()[2] = true;
      }
    }

    /**
      * Gets the value of the 'uri' field.
      * @return The value.
      */
    public java.lang.CharSequence getUri() {
      return uri;
    }

    /**
      * Sets the value of the 'uri' field.
      * @param value The value of 'uri'.
      * @return This builder.
      */
    public Energistics.Etp.v12.Protocol.GrowingObject.DeletePartsByRange.Builder setUri(java.lang.CharSequence value) {
      validate(fields()[0], value);
      this.uri = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'uri' field has been set.
      * @return True if the 'uri' field has been set, false otherwise.
      */
    public boolean hasUri() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'uri' field.
      * @return This builder.
      */
    public Energistics.Etp.v12.Protocol.GrowingObject.DeletePartsByRange.Builder clearUri() {
      uri = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'deleteInterval' field.
      * @return The value.
      */
    public Energistics.Etp.v12.Datatypes.Object.IndexInterval getDeleteInterval() {
      return deleteInterval;
    }

    /**
      * Sets the value of the 'deleteInterval' field.
      * @param value The value of 'deleteInterval'.
      * @return This builder.
      */
    public Energistics.Etp.v12.Protocol.GrowingObject.DeletePartsByRange.Builder setDeleteInterval(Energistics.Etp.v12.Datatypes.Object.IndexInterval value) {
      validate(fields()[1], value);
      this.deleteIntervalBuilder = null;
      this.deleteInterval = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'deleteInterval' field has been set.
      * @return True if the 'deleteInterval' field has been set, false otherwise.
      */
    public boolean hasDeleteInterval() {
      return fieldSetFlags()[1];
    }

    /**
     * Gets the Builder instance for the 'deleteInterval' field and creates one if it doesn't exist yet.
     * @return This builder.
     */
    public Energistics.Etp.v12.Datatypes.Object.IndexInterval.Builder getDeleteIntervalBuilder() {
      if (deleteIntervalBuilder == null) {
        if (hasDeleteInterval()) {
          setDeleteIntervalBuilder(Energistics.Etp.v12.Datatypes.Object.IndexInterval.newBuilder(deleteInterval));
        } else {
          setDeleteIntervalBuilder(Energistics.Etp.v12.Datatypes.Object.IndexInterval.newBuilder());
        }
      }
      return deleteIntervalBuilder;
    }

    /**
     * Sets the Builder instance for the 'deleteInterval' field
     * @param value The builder instance that must be set.
     * @return This builder.
     */
    public Energistics.Etp.v12.Protocol.GrowingObject.DeletePartsByRange.Builder setDeleteIntervalBuilder(Energistics.Etp.v12.Datatypes.Object.IndexInterval.Builder value) {
      clearDeleteInterval();
      deleteIntervalBuilder = value;
      return this;
    }

    /**
     * Checks whether the 'deleteInterval' field has an active Builder instance
     * @return True if the 'deleteInterval' field has an active Builder instance
     */
    public boolean hasDeleteIntervalBuilder() {
      return deleteIntervalBuilder != null;
    }

    /**
      * Clears the value of the 'deleteInterval' field.
      * @return This builder.
      */
    public Energistics.Etp.v12.Protocol.GrowingObject.DeletePartsByRange.Builder clearDeleteInterval() {
      deleteInterval = null;
      deleteIntervalBuilder = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /**
      * Gets the value of the 'includeOverlappingIntervals' field.
      * @return The value.
      */
    public java.lang.Boolean getIncludeOverlappingIntervals() {
      return includeOverlappingIntervals;
    }

    /**
      * Sets the value of the 'includeOverlappingIntervals' field.
      * @param value The value of 'includeOverlappingIntervals'.
      * @return This builder.
      */
    public Energistics.Etp.v12.Protocol.GrowingObject.DeletePartsByRange.Builder setIncludeOverlappingIntervals(boolean value) {
      validate(fields()[2], value);
      this.includeOverlappingIntervals = value;
      fieldSetFlags()[2] = true;
      return this;
    }

    /**
      * Checks whether the 'includeOverlappingIntervals' field has been set.
      * @return True if the 'includeOverlappingIntervals' field has been set, false otherwise.
      */
    public boolean hasIncludeOverlappingIntervals() {
      return fieldSetFlags()[2];
    }


    /**
      * Clears the value of the 'includeOverlappingIntervals' field.
      * @return This builder.
      */
    public Energistics.Etp.v12.Protocol.GrowingObject.DeletePartsByRange.Builder clearIncludeOverlappingIntervals() {
      fieldSetFlags()[2] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public DeletePartsByRange build() {
      try {
        DeletePartsByRange record = new DeletePartsByRange();
        record.uri = fieldSetFlags()[0] ? this.uri : (java.lang.CharSequence) defaultValue(fields()[0]);
        if (deleteIntervalBuilder != null) {
          record.deleteInterval = this.deleteIntervalBuilder.build();
        } else {
          record.deleteInterval = fieldSetFlags()[1] ? this.deleteInterval : (Energistics.Etp.v12.Datatypes.Object.IndexInterval) defaultValue(fields()[1]);
        }
        record.includeOverlappingIntervals = fieldSetFlags()[2] ? this.includeOverlappingIntervals : (java.lang.Boolean) defaultValue(fields()[2]);
        return record;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<DeletePartsByRange>
    WRITER$ = (org.apache.avro.io.DatumWriter<DeletePartsByRange>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<DeletePartsByRange>
    READER$ = (org.apache.avro.io.DatumReader<DeletePartsByRange>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

}
